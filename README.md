# ubuntu-18.10-setup

# TODO

## Test Virtualbox MacOS

- iTunes
- Rekordbox
- Adobe stuff

## Android sync

- Rythmbox doesnt sync playlists.
- doubleTwist is for win only.
- banshee didn't install.
- Lollypop doesn't seem to support syncing.

## Backup system

TBD

## Photo library

TBD:
- digiKam
- Darktable
- Shotwell

## Adobe suite replacement

Test InDesign in wine: https://appdb.winehq.org/objectManager.php?sClass=application&iId=755

## Disk encryption

Use LUKS. But it is more complicated with dualboot:

https://medium.com/@chrishantha/encrypting-disks-on-ubuntu-19-04-b50bfc65182a
https://vitobotta.com/2018/01/11/ubuntu-full-disk-encryption-manual-partitioning-uefi/

# POSTPONED

## Music library

Rythmbox is shit. It cant sync playlist somehow and crashes with two songs already.
The others as well:

Clementine: unusable.
Amarok: KDE
Lollypop: no integrations...
Banshee: abandonned.

Best bet: Mixxx. Active community, lots of integrations, etc. But currently no way to sync to android and export to Rekordbox.

One important thing to note is Choones: https://www.choones.app/
It could potentially solve all those issues since it would be possible to sync between environments.

## Rekordbox

Fuck...
The only bet would be to run it on wine which currently doesn't work:
https://appdb.winehq.org/objectManager.php?sClass=version&iId=26945
https://www.linuxmusicians.com/viewtopic.php?t=20009

Options:

1. Stick with Rekordbox on MacOS/Windows. -> Implies that I need to sync music libraries between linux and MacOS/Win. This is hard.
2. Run Rekordbox in wine and play with the laptop again. shit.
3. Ditch Rekordbox alltogether and play using Mixxx and therefore with laptop.
4. Just put audio files on a usb stick without Reckordbox analysis.
5. Use the shitty Android App. TBD: How do I get my Playlists syncronized with the App?

# DONE

## Sleep when lid closed
https://itsfoss.com/ubuntu-close-lid-suspend/

AND:

https://medium.com/@laurynas.karvelis_95228/install-arch-linux-on-macbook-pro-11-2-retina-install-guide-for-year-2017-2034ceed4cb2

```
$ sudo nano /etc/udev/rules.d/90-xhc_sleep.rules
```

insert:
```# disable wake from S3 on XHC1
SUBSYSTEM=="pci", KERNEL=="0000:00:14.0", ATTR{power/wakeup}="disabled"
```

## Power management
http://www.webupd8.org/2013/04/improve-power-usage-battery-life-in.html
https://www.linuxuprising.com/2018/11/install-tlpui-in-ubuntu-or-linux-mint.html

Test: https://wiki.archlinux.org/index.php/Laptop_Mode_Tools

## InDesign

Dualboot, wine, Virtualbox or just fuck them east-side bitches that use this bullshit.
Ottherwise use linux software: https://www.scribus.net/

## Sketch

Many solutions: Figma, Zeplin, etc.

## Docker bug

```
$ pygmy up
Successfully started dnsmasq
Error response from daemon: driver failed programming external connectivity on endpoint amazeeio-haproxy (e96eef1ed4a722fd0390c83f794d815ac07a265033dd46ddd918fac1cc272cfb): Error starting userland proxy: listen tcp 0.0.0.0:80: bind: address already in use
Error: failed to start containers: amazeeio-haproxy
Traceback (most recent call last):
	9: from /usr/local/bin/pygmy:23:in `<main>'
	8: from /usr/local/bin/pygmy:23:in `load'
	7: from /var/lib/gems/2.5.0/gems/pygmy-0.9.11/bin/pygmy:309:in `<top (required)>'
	6: from /var/lib/gems/2.5.0/gems/thor-0.20.3/lib/thor/base.rb:466:in `start'
	5: from /var/lib/gems/2.5.0/gems/thor-0.20.3/lib/thor.rb:387:in `dispatch'
	4: from /var/lib/gems/2.5.0/gems/thor-0.20.3/lib/thor/invocation.rb:126:in `invoke_command'
	3: from /var/lib/gems/2.5.0/gems/thor-0.20.3/lib/thor/command.rb:27:in `run'
	2: from /var/lib/gems/2.5.0/gems/pygmy-0.9.11/bin/pygmy:35:in `up'
	1: from /var/lib/gems/2.5.0/gems/pygmy-0.9.11/bin/pygmy:131:in `exec_up'
/var/lib/gems/2.5.0/gems/pygmy-0.9.11/lib/pygmy/docker_service.rb:13:in `start': Failed to run amazeeio-haproxy.  Command docker run -d -p 80:80 -p 443:443 --volume=/var/run/docker.sock:/tmp/docker.sock --restart=always --name=amazeeio-haproxy amazeeio/haproxy failed (RuntimeError)
```

-> ask amazee.

Solution:
Find out which process is listening on the :80 port:
`$ sudo lsof -n -P -i4 -i6 |grep :80`

Stop apache2:
`$ sudo service apache2 stop`

Disable auto start of apache2:
`$ sudo update-rc.d apache2 disable`

## Performance
https://itsfoss.com/speed-up-ubuntu-1310/
`sudo apt-get install preload`

## Install yarn
```
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update
sudo apt install yarn
```

## Spotify scaling
Quick run using alt + fn + F2:
`spotify --force-device-scale-factor=2.5`

## GSConnect
- Install from Ubuntu Software
- Install KDE Connect on Android device
- Pair with device
- Solve all issues in the Android app.

## Switch between application windows
alt + KEY ABOVE TAB

## XDebug

Install browser exension:
https://addons.mozilla.org/en-US/firefox/addon/xdebug-helper-for-firefox/

Follow PHPStorm instructions:
https://www.jetbrains.com/help/phpstorm/2019.2/zero-configuration-debugging.html?utm_campaign=PS&utm_medium=link&utm_source=product&utm_content=2019.2

Remove the line in docker-compose.yml:
`DOCKERHOST: host.docker.internal`

## Mounting APFS

https://linuxnewbieguide.org/how-to-mount-macos-apfs-disk-volumes-in-linux/

Creating mount point:
`sudo mkdir -p /media/$USERNAME/macos`

Mounting drive:

`sudo apfs-fuse -o allow_other /dev/sda2 /media/luksak/macos/`

## Disable animations

`sudo apt install gnome-tweak-tool`
Then disable animations in Tweaks -> General

## Bluetooth audio stuttering

Working after installing Blueman and always only using one audio application:

`sudo apt-get install blueman`

It seems that BT audio is strongly affected by Wifi activity as well.

## List hardware devices

`$ inxi -Fxzd`

## Fractional scaling

https://www.omgubuntu.co.uk/2019/06/enable-fractional-scaling-ubuntu-19-04

`$ gsettings set org.gnome.mutter experimental-features "['x11-randr-fractional-scaling']"`

## ZSH / Oh My ZSH

https://ohmyz.sh/

```
$ sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

## List installed packages

`apt list --installed`

## Fixing audio

`sudo nano /etc/modprobe.d/modprobe.conf`

Put:

`options snd_hda_intel model=intel-mac-auto`

## Backlight

### SOLVED

https://wiki.archlinux.org/index.php/MacBookPro11,x#Screenbacklight

`$ sudo nano /etc/rc.local`

Put:

`setpci -v -H1 -s 00:01.00 BRIDGE_CONTROL=0`

Then:

`sudo chmod 755 /etc/rc.local`

### Previous:

Bug report:

https://bugs.launchpad.net/ubuntu/+source/nvidia-graphics-drivers-384/+bug/1720438

A possible but complicated workaround is to disable Wayland an use Xorg:
https://askubuntu.com/questions/1010405/the-brightness-of-laptop-screen-cannot-be-adjusted-with-either-the-buttons-or-th/1013767#1013767

Not working:
https://askubuntu.com/questions/812734/macbook-pro-backlight-control-not-working-on-ubuntu-16-04
https://askubuntu.com/questions/1057783/no-brightness-control-18-04-lts-sys-class-backlight-is-empty/1152763

## List processes listeing on port
```
$ sudo lsof -n -P -i4 -i6 |grep :443 
```
or
```
$ sudo fuser 443/tcp
```

## Kill processes listeing on port 
```
$ sudo fuser -k 443/tcp
```