# Installation
## Copy /home folder
## 1Password
- go to https://1password.com/de/downloads/linux/ and download the FF extension
- login

## Install software

### Log in to online accounts using the Ubuntu wizard



### ZSH

```
$ sudo apt install zsh powerline fonts-powerline
$ chsh -s $(which zsh)
$ reboot
$ sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

### Install packages
```
$ sudo apt install htop php preload blueman gnome-shell-extension-gsconnect apfs-fuse hfsprogs gnome-tweaks opera inkscape tlp
```

### Franz
Download image & open in software install app:
https://meetfranz.com/de//download?platform=linux&type=deb

### Set up email accounts in Thunderbird

Enable menu bar -> in hamburger on top right corner -> preferences
View -> Layout -> Vertical view
View -> Folders -> Unified

### Nice to have
```
$ sudo snap install spotify
$ sudo snap install slack --classic
$ sudo snap install phpstorm --classic
```

### Firefox GPU acceleration
https://cialu.net/enable-hardware-acceleration-firefox-make-faster/

In `about:config` enable `layers.acceleration.force-enabled`

### Work
#### Docker
Check again here regarding the keys, etc:
https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository

```
$ sudo apt-get update
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo apt-key fingerprint 0EBFCD88
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
$ sudo apt-get update
$ sudo apt-get install docker-ce docker-ce-cli containerd.io

```
Fix running docker as sudo and therefore stuff like `$ pygmy up` failing:
```
$ sudo usermod -a -G docker $USER
$ reboot
```

### Docker Compose
https://docs.docker.com/compose/install/


```
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose
```

### Pygmy

```
$ sudo apt install ruby-full
$ sudo gem install pygmy
$ pygmy up
```
### PHP
```
$ sudo apt install php
```

Disable autostart of apache:

```
$ sudo service apache2 stop
$ sudo update-rc.d apache2 disable
```

Install some important modules:

```
$ sudo apt-get install php-curl php-zip php-mbstring php-gd php-dom php-bcmath
```

#### Installing multiple versions of PHP

https://hostadvice.com/how-to/how-to-switch-between-php-versions-on-an-ubuntu-18-04-vps-or-dedicated-server/


```
$ sudo apt-add-repository ppa:ondrej/php
$ sudo apt install -y php7.1 php7.1-cli php7.1-common
$ sudo apt-get install php-curl php-zip php-mbstring php-gd php-dom php-bcmath
$ sudo update-alternatives --config php
$ php -v
```

#### Composer:
Copy-paste code from here:
https://getcomposer.org/download/

Then run this to make it global:
```
$ sudo mv composer.phar /usr/local/bin/composer
```

### NVM

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.0/install.sh | bash
```

### Yarn
```
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update
sudo apt install yarn
```

### For old projects: Grunt & sass

```
$ npm i -g grunt
$ sudo gem install sass compass susy breakpoint breakpoint-slicer sass-globbing
```

## File systems

### APFS
https://linuxnewbieguide.org/how-to-mount-macos-apfs-disk-volumes-in-linux/

```
$ sudo apt update
$ sudo apt install fuse libfuse-dev libicu-dev bzip2 libbz2-dev cmake clang git libattr1-dev zlib1g-dev libfuse3-dev

$ git clone https://github.com/sgan81/apfs-fuse.git
$ cd apfs-fuse
$ git submodule init
$ git submodule update

$ mkdir build
$ cd build
$ cmake ..
$ make

$ sudo cp apfs-* /usr/local/bin

$ sudo mkdir -p /media/$USERNAME/macos
```

Automatically mount macos partition:

```
$ sudo nano /etc/rc.local
```
Put:
```
$ sudo apfs-fuse -o allow_other /dev/sda2 /media/luksak/macos/
```

## Hardware
### Backlight

https://wiki.archlinux.org/index.php/MacBookPro11,x#Screenbacklight

`$ sudo nano /etc/rc.local`

Put:

`setpci -v -H1 -s 00:01.00 BRIDGE_CONTROL=0`

Then:

`sudo chmod 755 /etc/rc.local`

### UNSURE IF NEEDED: Enable nvidia opencl to actually get better performance
```
$ sudo apt-get install nvidia-opencl-dev 
```

### Fixing fan controls (Huge performance win!)
```
$ sudo apt-get install mbpfan
```


#### Harddrive performance issues (important!)

http://phpforus.com/how-to-make-mysql-run-fast-with-ext4-on-ubuntu/

### Apple Aluminium Keyboard
https://bugs.launchpad.net/ubuntu/+source/gnome-control-center/+bug/1043336

Temporary fix that is broken again after restart:
```
echo 0 | sudo tee /sys/module/hid_apple/parameters/iso_layout
```

### Konica Minolta bizhub printer

* Download driver in the downloads section: https://www.konicaminolta.co.uk/en-gb/hardware/office/bizhub-c308#specifications

* Run the installation: `$ sudo ./install.pl`
* Reboot

* Add the printer in CUPS: http://localhost:631/
* Choose `Internet Printing Protocol (ipp)`
* Connection: ipp://192.168.27.48:631/ipp
* Upload the PPD from the driver download: `beu36C-9ux.ppd`
* Then set up the account tracking
